# Example Configurations for Server and Client Hardening

**NOTE: The following configuration files are provided AS-IS and WITHOUT WARRANTY of any imaginable kind whatsoever.
YOU MUST KNOW what you are doing and check and adjust the configuration for yourself before applying any changes.
YOU are responsible for any effect resulting from applying the configurations.**

## References

- <https://ssl-config.mozilla.org/>
- <https://observatory.mozilla.org/>
- <https://www.ssllabs.com/ssltest/>
- <https://securityheaders.com/>
- <https://github.com/sigio/cipherli.st>
- <https://cipherlist.eu/>
- <https://webdock.io/en/docs/how-guides/how-to-configure-security-headers-in-nginx-and-apache>
- <https://infosec.mozilla.org/>
- <https://www.w10privacy.de>
- <https://www.oo-software.com/en/shutup10>
- <https://www.henrypp.org/product/simplewall>
- <https://tools.google.com/dlpage/gaoptout>
- <https://privacybadger.org/>
- <https://ublockorigin.com/>
- <https://www.eff.org/https-everywhere>
- <https://restoreprivacy.com/firefox-privacy/>


// Firefox privacy & security settings
// put into your profile directory (see profiles.ini)
// cat profiles.ini | grep Default | head -1 | cut -d'=' -f2
// https://restoreprivacy.com/firefox-privacy/

user_pref("browser.contentblocking.category", "custom");

// safebrowsing
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);


// telemetry
pref("toolkit.telemetry.ecosystemtelemetry.enabled", false);
user_pref("toolkit.telemetry.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("browser.discovery.enabled", false); // via healthreport
user_pref("app.shield.optoutstudies.enabled", false); // via healthreport

// dom & https only
user_pref("dom.security.https_only_mode", true);
user_pref("dom.security.https_only_mode_ever_enabled", true);
user_pref("dom.event.clipboardevents.enabled", false);

// privacy & fingerprinting & tracking
user_pref("privacy.resistFingerprinting", true);
user_pref("privacy.donottrackheader.enabled", true);
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.fingerprinting.enabled", true);
user_pref("privacy.trackingprotection.socialtracking.enabled", true);
user_pref("privacy.trackingprotection.annotate_channels", true);
user_pref("privacy.trackingprotection.cryptomining.enabled", true);

// security
user_pref("security.tls.enable_post_handshake_auth", true);

// cookies
user_pref("privacy.firstparty.isolate", true);
user_pref("network.cookie.lifetimePolicy", 2);
user_pref("network.cookie.cookieBehavior", 4);

// prefetch & prediction
user_pref("network.dns.disablePrefetch", true);
user_pref("network.prefetch-next", false);
user_pref("network.predictor.cleaned-up", true);
user_pref("network.predictor.enabled", false);

// services
user_pref("geo.enabled", false);
